import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../widgets/category_item.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
        // Scaffold(
        //   appBar: AppBar(
        //     title: const Text('DeliMeal'),
        //   ),
        //   body:
        GridView(
      padding: const EdgeInsets.all(25),
      children: DUMMY_CATEGORIES
          .map(
            (catData) => CategoryItem(catData),
          )
          .toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200, // max height
        childAspectRatio: 3 / 2, // aspect ratio
        crossAxisSpacing: 20, // horizontal space
        mainAxisSpacing: 20, // vertical space
      ),
    );

    // );
  }
}
