import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class FiltersScreen extends StatefulWidget {
  static const routeName = '/filtersScreen';

  final Map<String, bool> currentFilterData;
  final Function setFilterData;

  FiltersScreen(this.currentFilterData, this.setFilterData);

  @override
  _FiltersScreenState createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  bool _glutinFree = false;
  bool _vegetarian = false;
  bool _vegan = false;
  bool _lactosFree = false;

  @override
  initState() {
    _glutinFree = widget.currentFilterData['glutin-free'];
    _vegetarian = widget.currentFilterData['vegetarian'];
    _vegan = widget.currentFilterData['vegan'];
    _lactosFree = widget.currentFilterData['lactos-free'];

    super.initState();
  }

  Container buildSwitchContainer(
      {String title, String subtitle, bool oldValue, Function handler}) {
    return Container(
      child: SwitchListTile(
        value: oldValue,
        title: Text(title),
        subtitle: Text(subtitle),
        onChanged: handler,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Filter'),
        actions: [
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                Map<String, bool> _filterData = {
                  'glutin-free': _glutinFree,
                  'vegetarian': _vegetarian,
                  'vegan': _vegan,
                  'lactos-free': _lactosFree,
                };
                widget.setFilterData(_filterData);
              }),
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20.0),
            child: Text(
              'Adjust your meal selection',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                buildSwitchContainer(
                    title: 'Glutin-Free',
                    subtitle: 'Only includ glutin-free meals',
                    oldValue: _glutinFree,
                    handler: (newValue) {
                      setState(() {
                        _glutinFree = newValue;
                      });
                    }),
                Divider(),
                buildSwitchContainer(
                    title: 'Vegetarian',
                    subtitle: 'Only includ vegetarian meals',
                    oldValue: _vegetarian,
                    handler: (newValue) {
                      setState(() {
                        _vegetarian = newValue;
                      });
                    }),
                Divider(),
                buildSwitchContainer(
                    title: 'Vegan',
                    subtitle: 'Only includ vegan meals',
                    oldValue: _vegan,
                    handler: (newValue) {
                      setState(() {
                        _vegan = newValue;
                      });
                    }),
                Divider(),
                buildSwitchContainer(
                    title: 'Lactos Free',
                    subtitle: 'Only includ lactos-free meals',
                    oldValue: _lactosFree,
                    handler: (newValue) {
                      setState(() {
                        _lactosFree = newValue;
                      });
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
