import 'package:flutter/material.dart';

import '../models/category.dart';
import '../models/Meal.dart';
import '../widgets/meal_item.dart';

class CategoriesMealsScreen extends StatefulWidget {
  static const routeName = '/categoriesMealsScreen';

  final List<Meal> mealData;

  CategoriesMealsScreen(this.mealData);

  @override
  _CategoriesMealsScreenState createState() => _CategoriesMealsScreenState();
}

class _CategoriesMealsScreenState extends State<CategoriesMealsScreen> {
  Category categoryObj;
  List<Meal> arrMeal;
  var isLoadedData = false;

  @override
  void initState() {
    // TODO: this method will call once load, context is not available here
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!isLoadedData) {
      categoryObj = ModalRoute.of(context).settings.arguments as Category;
      String categoryId = categoryObj.id;
      arrMeal = widget.mealData.where((meal) {
        return meal.categories.contains(categoryId);
      }).toList();
      isLoadedData = true;
    }
    super.didChangeDependencies();
  }

  // void removeCategory(Meal objMeal) {
  //   setState(() {
  //     arrMeal.removeWhere((mealObj) => mealObj.id == objMeal.id);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    String categoryTitle = categoryObj.title;

    return Scaffold(
      appBar: AppBar(
        title: Text('$categoryTitle'),
      ),
      body: ListView.builder(
        itemBuilder: (ctx, index) {
          return MealItem(arrMeal[index]); //Text(arrMeal[index].title);
        },
        itemCount: arrMeal.length,
      ),
    );
  }
}
