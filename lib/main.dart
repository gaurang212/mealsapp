import 'package:flutter/material.dart';

import 'screens/categories_meals_screen.dart';
import 'screens/categories_screen.dart';
import 'screens/meal_detail_screen.dart';
import 'screens/tabs_screen.dart';
import 'screens/filters_screen.dart';
import 'dummy_data.dart';
import './models/Meal.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filterData = {
    'glutin-free': false,
    'vegetarian': false,
    'vegan': false,
    'lactos-free': false,
  };

  List<Meal> arrMeal = DUMMY_MEALS;
  List<Meal> favouriteMeal = [];

  void setFilterData(Map<String, bool> currentFilter) {
    _filterData = currentFilter;
    setState(() {
      arrMeal = DUMMY_MEALS.where((mealObj) {
        if (_filterData['glutin-free'] && !mealObj.isGlutenFree) {
          return false;
        }
        if (_filterData['vegetarian'] && !mealObj.isVegetarian) {
          return false;
        }
        if (_filterData['vegan'] && !mealObj.isVegan) {
          return false;
        }
        if (_filterData['lactos-free'] && !mealObj.isLactoseFree) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void toggleFavourite(String id) {
    int indexMeal = favouriteMeal.indexWhere((meal) => meal.id == id);

    if (indexMeal >= 0) {
      setState(() {
        favouriteMeal.removeAt(indexMeal);
      });
    } else {
      setState(() {
        favouriteMeal.add(DUMMY_MEALS.firstWhere((meal) => meal.id == id));
      });
    }
  }

  bool isFavouriteMeal(String id) {
    if (favouriteMeal.isEmpty) {
      return false;
    }
    return favouriteMeal.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'DeliMeals',
      debugShowCheckedModeBanner: false, // in order to hide that debug banner
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        accentColor: Colors.yellow,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(), // if we specify this with '/' then it will
      // start load from there
      initialRoute:
          '/', // no need to specify as by-default its value is '/' only
      routes: {
        '/': (context) => TabsScreen(favouriteMeal), //CategoriesScreen(),
        CategoriesMealsScreen.routeName: (context) =>
            CategoriesMealsScreen(this.arrMeal),

        MealDetailScreen.routeName: (context) => MealDetailScreen(
              toggleFavourite: this.toggleFavourite,
              isFavouriteMeal: this.isFavouriteMeal,
            ),

        FiltersScreen.routeName: (context) =>
            FiltersScreen(_filterData, setFilterData),
      },
      onGenerateRoute: (settings) {
        print('route args: $settings.args');

        // currently it generates crashes as we expect data on following way
        // and it is null
        // ModalRoute.of(context).settings.arguments as Category; -> this comes null

        // if (settings.name == '/categoriesMealsScreen') {
        //   return MaterialPageRoute(
        //       builder: (context) => CategoriesMealsScreen());
        // }
        return MaterialPageRoute(builder: (context) => CategoriesScreen());
      },
      onUnknownRoute: (settings) {
        print('route args: $settings.args');
        // useful for '404' page not found related handling, requires when
        // 'onGenerateRoute' not executed or provide required navigation action properly.
        return MaterialPageRoute(builder: (context) => CategoriesScreen());
      },
    );
  }
}
