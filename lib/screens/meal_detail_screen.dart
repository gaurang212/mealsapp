import 'package:flutter/material.dart';

import '../models/Meal.dart';

class MealDetailScreen extends StatelessWidget {
  static const routeName = '/mealDetailScreen';

  final Function toggleFavourite;
  final Function isFavouriteMeal;

  MealDetailScreen({this.toggleFavourite, this.isFavouriteMeal});

  Widget buildHeaderLabel(BuildContext context, String title) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Text(
        title,
        style: Theme.of(context).textTheme.headline6,
      ),
    );
  }

  Widget buildContainerForList({Widget child}) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      height: 200.0,
      width: 300.0,
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: child,
    );
  }

  @override
  Widget build(BuildContext context) {
    final mealObj = ModalRoute.of(context).settings.arguments as Meal;
    final title = mealObj.title;
    // final id = args.id;

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.network(
                mealObj.imageUrl,
                height: 300,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              buildHeaderLabel(context, 'Ingredient'),
              buildContainerForList(
                child: ListView.builder(
                  itemBuilder: (ctx, index) {
                    return Card(
                      color: Theme.of(context).accentColor,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 10.0),
                        child: Text(mealObj.ingredients[index]),
                      ),
                    );
                  },
                  itemCount: mealObj.ingredients.length,
                ),
              ),
              buildHeaderLabel(context, 'Steps'),
              buildContainerForList(
                  child: ListView.builder(
                itemBuilder: (ctx, index) {
                  return Column(
                    children: [
                      ListTile(
                        leading: CircleAvatar(
                          backgroundColor: Theme.of(context).accentColor,
                          child: Text('#${index + 1}'),
                        ),
                        title: Text('${mealObj.steps[index]}'),
                      ),
                      Divider(),
                    ],
                  );
                },
                itemCount: mealObj.steps.length,
              )),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: isFavouriteMeal(mealObj.id)
            ? Icon(Icons.star)
            : Icon(Icons.star_border),
        onPressed: () {
          // Navigator.of(context).pop(mealObj);
          toggleFavourite(mealObj.id);
        },
      ),
    );
  }
}
