import 'package:flutter/material.dart';
import '../screens/categories_meals_screen.dart';
import '../models/category.dart';

class CategoryItem extends StatelessWidget {
  // final String title;
  // final Color color;
  final Category categoryObj;

  CategoryItem(this.categoryObj);

  void goToNext(BuildContext ctx) {
    // Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
    //   return CategoriesMealsScreen(categoryObj);
    // }));

    Navigator.of(ctx)
        .pushNamed(CategoriesMealsScreen.routeName, arguments: categoryObj);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => goToNext(context),
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          categoryObj.title,
          style: Theme.of(context).textTheme.headline6,
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              categoryObj.color.withOpacity(0.7),
              categoryObj.color,
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
