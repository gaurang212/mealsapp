import 'package:flutter/material.dart';

import 'categories_screen.dart';
import 'favourites_screen.dart';
import '../widgets/main_drawer.dart';

import '../models/Meal.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> favouriteMeal;

  TabsScreen(this.favouriteMeal);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Widget> pages;

  int _selectedIndex = 0;

  @override
  void initState() {
    pages = [
      CategoriesScreen(),
      FavouritesScreen(widget.favouriteMeal),
    ];
    super.initState();
  }

  void _setSelectedScreen(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  String screenName() {
    if (_selectedIndex == 0) {
      return "Categories";
    } else {
      return "Favourites";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(screenName()),
      ),
      drawer: MainDrawer(),
      body: pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _setSelectedScreen,
        backgroundColor: Theme.of(context).primaryColor,
        selectedItemColor: Theme.of(context).accentColor,
        unselectedItemColor: Colors.white,
        currentIndex: _selectedIndex,
        // type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(
              Icons.category,
            ),
            label: 'Categories',
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(
              Icons.star,
            ),
            label: 'Favourites',
          ),
        ],
      ),
    );
  }
}

// For Top Tab bar item style
// class _TabsScreenState extends State<TabsScreen> {
//   @override
//   Widget build(BuildContext context) {
//     return DefaultTabController(
//       length: 2,
//       initialIndex: 0,
//       child: Scaffold(
//         appBar: AppBar(
//           title: Text('Meals'),
//           bottom: TabBar(tabs: <Widget>[
//             Tab(
//               icon: Icon(Icons.category),
//               text: 'Categories',
//             ),
//             Tab(
//               icon: Icon(Icons.star),
//               text: 'Favourites',
//             ),
//           ]),
//         ),
//         body: TabBarView(children: <Widget>[
//           CategoriesScreen(),
//           FavouritesScreen(),
//         ]),
//       ),
//     );
//   }
// }
